'use strict';

module.exports = {
  phantom: require('../tags/phantom/phantom.tag'),
  header: require('../tags/header/header.tag'),
  preview: require('../tags/preview/preview.tag'),
  interval: require('../tags/interval/interval.tag'),
  hex: require('../tags/hex/hex.tag'),
  rgb: require('../tags/rgb/rgb.tag'),
  hsv: require('../tags/hsv/hsv.tag'),
  draw: require('../tags/draw/draw.tag')
};
