<ri-header>
  <header class="header">
    <section class="panel-blank">
      <img src="/img/logo-blank.svg" class="logo">
      <img src="/img/logo-notext-blank.svg" class="logo-notext">

      <ri-preview class="header-preview"></ri-preview>
    </section>
  </header>
</ri-header>
