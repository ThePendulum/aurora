'use strict';

module.exports = {
  chip: 'ws2812b',
  size: [16, 16],
  zigzag: true,
  colorIndex: [0, 1, 2],
  regulator: .5,
  /*
  chip: 'ws2801',
  size: 160,
  zigzag: false,
  colorIndex: [0, 2, 1],
  regulator: 1,
  */
  web: {
    port: 3000
  },
  socket: {
    port: 3001,
    previewUpdateInterval: 200
  }
};
